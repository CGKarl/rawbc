# Rawbc
![](https://img.shields.io/badge/version-1.1-brightgreen.svg)

Rawbc is a spigot plugin that helps you to test and find good-looking messages before setting them in your configs.

## Download
You can download the latest version from the [download page](https://bitbucket.org/CGKarl/rawbc/downloads/?tab=downloads) or using this [direct download link](https://bitbucket.org/CGKarl/rawbc/downloads/Rawbc.jar).

## Usage
### Default
Usage: `/rawbc <message>` or `/rawbroadcast <message>`

This will broadcast your message without a prefix.

You can also use color codes using `&`.

A list with all colors can be found [here](https://minecraft.gamepedia.com/Formatting_codes).

### Centered messages
Usage: `/rawbc -c <message>` or `/rawbroadcast -c <message>`

This will broadcast your message but centered. Example:

![](https://proxy.spigotmc.org/88b12c608571fa7a7462d36e4c2897bd37deb781?url=https%3A%2F%2Fi.gyazo.com%2F13c04f91a7af7a35905504ab6e207d9a.png)

Everything else is equal to the default usage.

**Note that the permission node `rawbc.use` or op is required to use these commands.**

## Credit
More information about centered messages can be found on this [spigotmc forum post](https://www.spigotmc.org/threads/free-code-sending-perfectly-centered-chat-message.95872/).