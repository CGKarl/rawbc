package de.cgkarl.rawbc;

import de.cgkarl.rawbc.advanced.CenterUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.command.Command;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.plugin.Description;
import org.bukkit.plugin.java.annotation.plugin.Plugin;
import org.bukkit.plugin.java.annotation.plugin.author.Author;

import java.util.Arrays;

@Plugin(name = "Rawbc", version = "1.1")
@Author(name = "CGKarl")
@Description(desc = "A tools that helps you designing your server")
@Command(name = "rawbc", aliases = {"rawbroadcast"}, desc = "Broadcast command", permission = "rawbc.use")
@Permission(name = "rawbc.use", desc = "Allows broadcast command", defaultValue = PermissionDefault.OP)
public class Rawbc extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getCommand("rawbc").setExecutor((commandSender, command, label, args) -> {
            if (args.length == 0) {
                commandSender.sendMessage("§cUsage: /" + label + " [-c] <message>");
                return true;
            }

            String message = (args[0].equalsIgnoreCase("-c") ?
                    CenterUtils.centerMessage(StringUtils.join(Arrays.copyOfRange(args, 1, args.length), ' ')) :
                    ChatColor.translateAlternateColorCodes('&', StringUtils.join(args, ' ')));

            Bukkit.broadcastMessage(message);

            return true;
        });
    }

}