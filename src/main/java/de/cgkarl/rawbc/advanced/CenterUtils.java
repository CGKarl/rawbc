package de.cgkarl.rawbc.advanced;

import lombok.NonNull;
import lombok.experimental.UtilityClass;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Collection;

/**
 * Utility class for sending centered texts to players
 * or groups of players.
 *
 * @author  SirSpoodles
 * @author  CGKarl
 *
 * @see     <a href="https://www.spigotmc.org/threads/free-code-sending-perfectly-centered-chat-message.95872/">Spigot Forum Post</a>
 */
@UtilityClass
public class CenterUtils {

    private final static int CENTER_PX = 154;

    /**
     * Centers the given text assuming the default chat size is used
     * and sends the centered message to the player.
     * The message cannot be <code>null</code>.
     *
     * @param player    the player who will receive the centered message
     * @param message   the message that will be centered
     *
     * @see             #centerMessage(String)
     */
    public void sendCenteredMessage(Player player, String message) {
        player.sendMessage(centerMessage(message));
    }

    /**
     * Centers the given text once assuming the default chat size is used
     * and sends the centered message to the given players.
     * The message cannot be <code>null</code>.
     *
     * @param players   the collection of players who will receive the centered message
     * @param message   the message that will be centered
     *
     * @see             #centerMessage(String)
     */
    public void sendCenteredMessage(Collection<? extends Player> players, String message) {
        String centeredMessage = centerMessage(message);

        players.forEach(player -> player.sendMessage(centeredMessage));
    }

    /**
     * Centers the given text assuming the default chat size is used.
     * The message cannot be <code>null</code>.
     *
     * @param message   the message that will be centered
     * @return          the centered message
     */
    public String centerMessage(@NonNull String message) {
        String coloredMessage = ChatColor.translateAlternateColorCodes('&', message);
        int messageWidth = 0;
        boolean previousCode = false;
        boolean isBold = false;

        if (message.length() == 0) {
            return coloredMessage;
        }

        for (char character : coloredMessage.toCharArray()) {
            if (character == '§') {
                previousCode = true;
            } else if (previousCode) {
                previousCode = false;
                isBold = (character == 'l' || character == 'L');
            } else {
                FontInfo fontInfo = FontInfo.getByChar(character);

                messageWidth += (isBold ? fontInfo.getBoldWidth() : fontInfo.getWidth()) + 1;
            }
        }

        StringBuilder centeredMessageBuilder = new StringBuilder();
        int toCompensate = CENTER_PX - (messageWidth / 2);

        for (int i = 0; i < toCompensate; i++) {
            centeredMessageBuilder.append(" ");
            i += FontInfo.SPACE.getWidth();
        }

        centeredMessageBuilder.append(coloredMessage);

        return centeredMessageBuilder.toString();
    }

}